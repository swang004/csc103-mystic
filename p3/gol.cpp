/*
 * CSc103 Project 3: Game of Life
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References: C plus plus, youtube and overstack.
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 48 hours
 */

#include <cstdio>
#include <stdlib.h> // for exit();
#include <getopt.h> // to parse long arguments.
#include <unistd.h> // sleep
#include <vector>
using std::vector;
#include <string>
using std::string;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Text-based version of Conway's game of life.\n\n"
"   --seed,-s     FILE     read start state from FILE.\n"
"   --world,-w    FILE     store current world in FILE.\n"
"   --fast-fw,-f  NUM      evolve system for NUM generations and quit.\n"
"   --help,-h              show this message and exit.\n";

size_t max_gen = 0; /* if > 0, fast forward to this generation. */

string wfilename =  "/tmp/gol-world-current"; /* write state here */

FILE* fworld = 0; /* handle to file wfilename. */

string initfilename = "/tmp/gol-world-current"; /* read initial state from here. */

size_t nbrCount(size_t i, size_t j, const vector<vector<bool> >& g);

void update();

int initFromFile(const string& fname);

void mainLoop();

void dumpState(FILE* f);

char text[3] = ".O";

int main(int argc, char *argv[]) {
	// define long options

	static struct option long_opts[] = {
		{"seed",    required_argument, 0, 's'},
		{"world",   required_argument, 0, 'w'},
		{"fast-fw", required_argument, 0, 'f'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};

	// process options:
	char c;
	int opt_index = 0;

	while ((c = getopt_long(argc, argv, "hs:w:f:", long_opts, &opt_index)) != -1) {

		switch (c) {

			case 'h': printf(usage,argv[0]);
				return 0;

			case 's': initfilename = optarg;
				break;

			case 'w': wfilename = optarg;
				break;

			case 'f': max_gen = atoi(optarg);
				break;

			case '?': printf(usage,argv[0]);
				return 1;
		}
	}

	mainLoop();
	return 0;
}

void mainLoop() {
	/* update, write, sleep */

String fn = “.”;
if( wfilename == fn)
	fworld = fopen(initfilename.c_str(), “wb”); // wb might be wrong

else fworld = fopen(wfilename.c_str(), “wb”);
for (size_t i = 0; i <= max_gen; ++i){
system(“clear”);
display(gamecopy); // gamecopy
dumpState(fworld);
If (i != max_gen)
update();
sleep(1);
}
fclose(fworld);

}

//--Grid  

size_t nbrCount(size_t i, size_t j, const vector<vector<bool> >& g){ //relocated the grind

int count = 0;
vector<vector<bool> >gamecopy;

if (gamecopy[i][j-1]==true){
count ++;}
if (gamecopy[i][j+1]==true)
{
count ++;}
if (gamecopy[i-1][j]==true) // i-1 not j-1
{
count ++;}
if (gamecopy[i+1][j]==true)
{
count ++;}
	
if(gamecopy[i-1][j-1] == true){
	count++;}
	
if(gamecopy[i-1][j+1] == true){
	count++;}
	
if(gamecopy[i+1][j+1] == true){
	count++;}
	
if(gamecopy[i+1][j-1] == true){
	count++;}

}

// void generation(vector<vector<bool> >game, vector<vector<bool> >gamecopy){

void update () {
	vector <vector<bool>  >  temp(row_size, vector <bool>(col_size,false)); 
	size _t count =0;
	for ( size_t i=0; i < row_size; ++i){
		for (size_t j=0;j< col_size; ++j)  {
			count = (nbrCount (i,j, gamecopy ));
				if (  (count<2 && count>3 && gamecopy[i][j] == false) {
					temp [i][j]=false;
				}
		else if (count == 3 && gamecopy[i][j] == true) {
		temp [i][j]= true;
		
		}

		else() { temp[i][j] = false; }

		}
		
		gamecopy=temp;

		}

void display(vector<vector<bool> > &g){ 
	for(size_t i = 0; i < game.size(); i++) {
		for(size_t j = 0; j < game.size(); j++){
			if(game[i][j]==true) {
				cout << "O";
				}
			else() {
				cout << "."; 
					}
					}
				}

int initFromFile(const string& fname) { // open and read file
	
	FILE* f  = fopen("/p3/res/glider-40x20", "rb"); // open file
	char c; //a place to store all the byte that will be read
	fread(&c,1,1,f); //read byte by byte and place it into c
	fclose(f);   //close file
	
	}

//-------------

void dumpstate(FILE* f){ // open and write files
    FILE* f = fopen("/p3/res/glider-40x20", "wb");  // open file
	char c = '.'; // write . to file
	fwrite(&c,1,1,f); //write f in byte
	fclose(f);  //close file
	}


}


